//package app.grm.katchme.view;
//
//import androidx.appcompat.app.AppCompatActivity;
//import androidx.core.content.res.ResourcesCompat;
//import androidx.recyclerview.widget.LinearLayoutManager;
//import androidx.recyclerview.widget.RecyclerView;
//
//import android.content.Context;
//import android.graphics.Color;
//import android.graphics.Typeface;
//import android.os.Bundle;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.FrameLayout;
//import android.widget.ImageView;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import app.grm.katchme.R;
//import app.grm.katchme.adapters.SelfSettingAdapter;
//import app.grm.katchme.fragments.ReviewFragment;
//import app.grm.katchme.fragments.StoriesFragment;
//import app.grm.katchme.fragments.TagStoriesFragment;
//import app.grm.katchme.model.SelfSetting;
//import xyz.klinker.android.drag_dismiss.activity.DragDismissActivity;
//
//public class UserProfileActivity extends DragDismissActivity {
//    TextView storiesTv, reviewTv, tagTv, logoutTv;
//    private Context context;
//    ImageView popUpMenu, back;
//
//    RelativeLayout settingsMenu;
//    RecyclerView settingList;
//
//
//    @Override
//    public View onCreateContent(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
//        final View view = inflater.inflate(R.layout.fragment_profile, parent, false);
//        tagTv = view.findViewById(R.id.tagStoriesTv);
//        this.context = parent.getContext();
//        storiesTv = view.findViewById(R.id.storiesTv);
//        reviewTv = view.findViewById(R.id.reviewTv);
//        popUpMenu = view.findViewById(R.id.popup_menu);
//        settingsMenu = view.findViewById(R.id.settings_menu);
//        logoutTv = view.findViewById(R.id.logout);
//
//        back = view.findViewById(R.id.back);
//        settingList = view.findViewById(R.id.settings);
//        settingList.setLayoutManager(new LinearLayoutManager(this));
//        settingList.setAdapter(new SelfSettingAdapter(this, getSettings()));
//
//        popUpMenu.setOnClickListener(view14 -> {
//            showActions();
//        });
//
//        back.setOnClickListener(view15 -> {
//            hideActions();
//        });
//
//        switchTvAndFragment("review");
//        tagTv.setOnClickListener(view1 -> {
//            switchTvAndFragment("tag");
//        });
//        storiesTv.setOnClickListener(view12 -> {
//            switchTvAndFragment("stories");
//        });
//        reviewTv.setOnClickListener(view13 -> {
//            switchTvAndFragment("review");
//        });
//        return view;
//    }
//
//    private void showActions() {
//        settingsMenu.setVisibility(View.VISIBLE);
//
//    }
//
//    private void hideActions() {
//        settingsMenu.setVisibility(View.GONE);
//    }
//
//    private List<SelfSetting> getSettings() {
//        List<SelfSetting> selfSettings = new ArrayList<>();
//        selfSettings.add(new SelfSetting("PASSWORT", 1));
//        selfSettings.add(new SelfSetting("KONTO", 2));
//        selfSettings.add(new SelfSetting("BLOCKIERTE PERSONEN", 3));
//        selfSettings.add(new SelfSetting("SUCHVERLAUF LOSCHEN", 4));
//        selfSettings.add(new SelfSetting("HILFE / KONTAKT", 5));
//        selfSettings.add(new SelfSetting("DATENSCHUTD", 6));
//        selfSettings.add(new SelfSetting("IMPRESSUM", 7));
//
//        return selfSettings;
//
//
//    }
//
//    private void switchTvAndFragment(String type) {
//
//        if (type.equals("review")) {
//            getSupportFragmentManager().beginTransaction().replace(R.id.container, new ReviewFragment())
//                    .commit();
//            reviewTv.setTextColor(Color.parseColor("#000000"));
//            reviewTv.setTextSize(25);
//            reviewTv.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/din_bold.ttf"));
//
//            storiesTv.setTextColor(Color.parseColor("#E0E0E0"));
//            storiesTv.setTextSize(17);
//            storiesTv.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/dinregular.ttf"));
//
//            // storiesTv.setTypeface(ResourcesCompat.getFont(context, R.font.dinregular));
//
//            tagTv.setTextColor(Color.parseColor("#E0E0E0"));
//            tagTv.setTextSize(17);
//            tagTv.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/dinregular.ttf"));
//
//            // tagTv.setTypeface(ResourcesCompat.getFont(context, R.font.dinregular));
//
//
//        } else if (type.equals("stories")) {
//            getSupportFragmentManager().beginTransaction().replace(R.id.container, new StoriesFragment())
//                    .commit();
//            storiesTv.setTextColor(Color.parseColor("#000000"));
//            storiesTv.setTextSize(25);
//            storiesTv.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/din_bold.ttf"));
//
////            storiesTv.setTypeface(ResourcesCompat.getFont(context, R.font.din_bold));
//
//            reviewTv.setTextColor(Color.parseColor("#E0E0E0"));
//            reviewTv.setTextSize(17);
//            reviewTv.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/dinregular.ttf"));
//
//            // reviewTv.setTypeface(ResourcesCompat.getFont(context, R.font.dinregular));
//
//
//            tagTv.setTextColor(Color.parseColor("#E0E0E0"));
//            tagTv.setTextSize(17);
//            tagTv.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/dinregular.ttf"));
//
//            // tagTv.setTypeface(ResourcesCompat.getFont(context, R.font.dinregular));
//
//        } else {
//            getSupportFragmentManager().beginTransaction().replace(R.id.container, new TagStoriesFragment())
//                    .commit();
//            tagTv.setTextColor(Color.parseColor("#000000"));
//            tagTv.setTextSize(25);
//            tagTv.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/din_bold.ttf"));
//
//            // tagTv.setTypeface(ResourcesCompat.getFont(context, R.font.din_bold));
//            reviewTv.setTextColor(Color.parseColor("#E0E0E0"));
//            reviewTv.setTextSize(17);
//            reviewTv.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/dinregular.ttf"));
//
//            // reviewTv.setTypeface(ResourcesCompat.getFont(context, R.font.dinregular));
//            storiesTv.setTextColor(Color.parseColor("#E0E0E0"));
//            storiesTv.setTextSize(17);
//            storiesTv.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/dinregular.ttf"));
//
//            //storiesTv.setTypeface(ResourcesCompat.getFont(context, R.font.dinregular));
//
//        }
//    }
//}
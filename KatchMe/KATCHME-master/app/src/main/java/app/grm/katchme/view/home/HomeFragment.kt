package app.grm.katchme.view.home

import android.graphics.Typeface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import app.grm.katchme.BaseFragment
import app.grm.katchme.R
import app.grm.katchme.view.Dashboard
import com.google.android.material.tabs.TabLayout

class HomeFragment : BaseFragment() {

    lateinit var tabs_main: TabLayout

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view1 = inflater.inflate(R.layout.fragment_home, container, false)
        tabs_main = view1.findViewById(R.id.tabs_main)

        setCurrentTabFragment(0)

        return view1
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        tabs_main.findViewById<TabLayout>(R.id.tabs_main)

        tabs_main.addTab(tabs_main.newTab(), true)
        tabs_main.addTab(tabs_main.newTab())
        tabs_main.addTab(tabs_main.newTab())

        setupTabIcons()
    }

    private fun setupTabIcons() {
        val tabOne = LayoutInflater.from(context).inflate(R.layout.custom_tabs, null) as TextView
        tabOne.text = "Katch"
        tabOne.setTypeface(null, Typeface.BOLD)
        //tabOne.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.item_tab_matches, 0)
        tabs_main.getTabAt(0)!!.customView = tabOne

        val tabTwo = LayoutInflater.from(context).inflate(R.layout.custom_tabs, null) as TextView
        tabTwo.text = "Feed"
        //tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.item_tab_glances, 0)
        tabs_main.getTabAt(1)!!.customView = tabTwo

        val tabThree = LayoutInflater.from(context).inflate(R.layout.custom_tabs, null) as TextView
        tabThree.text = "Map"
        //tabThree.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.item_tab_saved, 0)
        tabs_main.getTabAt(2)!!.customView = tabThree

        // Update Title
        tabs_main.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                val text = tab.customView as TextView
                text.setTypeface(null, Typeface.BOLD)
                text.setTextColor(ContextCompat.getColor(context!!, R.color.controllerColorBlueDark))

                setCurrentTabFragment(tab.position)
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
                val text = tab.customView as TextView
                text.setTypeface(null, Typeface.NORMAL)
                text.setTextColor(ContextCompat.getColor(context!!, R.color.controllerColorBlue))
            }

            override fun onTabReselected(tab: TabLayout.Tab) {
            }
        })
    }

    private fun setCurrentTabFragment(tabPosition: Int) {
        when (tabPosition) {
            0 -> {
                replaceFragment(KatchFragment())
                val activity = activity as Dashboard
                activity.showBottomView()
            }
            1 -> {
                val activity = activity as Dashboard
                activity.showBottomView()
                replaceFragment(FeedFragment())
            }
            2 -> {
                replaceFragment(MapFragment())
                val activity = activity as Dashboard
                activity.hideBottomView()
            }
        }
    }

    fun replaceFragment(fragment: Fragment) {
        val fm = activity!!.supportFragmentManager
        val ft = fm.beginTransaction()
        ft.replace(R.id.frame_container, fragment)
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        ft.commit()
    }
}
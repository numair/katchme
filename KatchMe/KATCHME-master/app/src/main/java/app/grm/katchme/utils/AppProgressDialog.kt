package app.grm.katchme.utils

//import android.widget.ImageView
//import com.bumptech.glide.Glide
//import com.bumptech.glide.request.target.GlideDrawableImageViewTarget
import android.app.Activity
import android.app.Dialog
import android.os.Handler
import android.view.Window
import android.view.WindowManager

/**
 * Created by Numair Qadir on 29/12/2018.
 */

//..we need the context else we can not create the dialog so get context in constructor
class AppProgressDialog(internal var activity: Activity) {
    var dialog: Dialog? = null

    fun showDialog() {

        dialog = Dialog(activity)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        //...set cancelable false so that it's never get hidden
        dialog!!.setCancelable(false)
        dialog!!.setCanceledOnTouchOutside(true)
        //...that's the layout i told you will inflate later
//        dialog!!.setContentView(R.layout.custom_progress_dialog)

        //...initialize the imageView form infalted layout
//        val gifImageView = dialog!!.findViewById(R.id.custom_loading_imageView) as ImageView

        /*
        it was never easy to load gif into an ImageView before Glide or Others library
        and for doing this we need DrawableImageViewTarget to that ImageView
        */
//
//        val imageViewTarget = GlideDrawableImageViewTarget(gifImageView)
//
        //...now load that gif which we put inside the drawble folder here with the help of Glide

//        Glide.with(activity)
//                .load(R.drawable.app_icon)
//                .placeholder(R.drawable.ic_man)
//                .centerCrop()
//                .crossFade()
//                .into(imageViewTarget)

        // Keep Screen active
        dialog!!.window?.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        //...finally show it
        dialog!!.show()
    }

    //..also create a method which will hide the dialog when some work is done
    fun hideDialog() {
        if (dialog != null && dialog!!.isShowing) {
            dialog!!.dismiss()
        }
    }
}
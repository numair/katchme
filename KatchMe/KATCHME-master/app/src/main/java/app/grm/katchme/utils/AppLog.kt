package app.grm.katchme.utils

import android.util.Log

/**
 * Created by Numair Qadir on 14/12/2018.
 */

object AppLog {
    private const val APP_TAG = "KatchMe"
    private const val isLogON = true

    fun message(message: String): Int {
        return if (isLogON) Log.i(APP_TAG, message) else 0
    }

    fun error(message: String): Int {
        return if (isLogON) Log.e(APP_TAG, message) else 0
    }

    fun information(message: String): Int {
        return if (isLogON) Log.i(APP_TAG, message) else 0
    }

    fun debug(message: String): Int {
        return if (isLogON) Log.d(APP_TAG, message) else 0
    }

    fun stackDebug(message: String): Int {
        return if (isLogON) Log.d(APP_TAG, message) else 0
    }

    fun error(tag: String, message: String): Int {
        return if (isLogON) Log.e(tag, message) else 0
    }

    fun information(tag: String, message: String): Int {
        return if (isLogON) Log.i(tag, message) else 0
    }

    fun debug(tag: String, message: String): Int {
        return if (isLogON) Log.d(tag, message) else 0
    }

    fun stackDebug(tag: String, message: String): Int {
        return if (isLogON) Log.d(tag + "_Stack", message) else 0
    }
}

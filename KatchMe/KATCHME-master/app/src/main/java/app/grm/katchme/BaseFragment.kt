package app.grm.katchme

import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import android.view.WindowManager
import androidx.fragment.app.Fragment
import app.grm.katchme.utils.AppProgressDialog
import com.google.firebase.auth.FirebaseAuth

/**
 * Created by Numair Qadir on 2019-10-26.
 */
open class BaseFragment : Fragment() {

    var progressDialog: AppProgressDialog? = null

    // Firebase
    var mFirebaseAuth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mFirebaseAuth = FirebaseAuth.getInstance()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        adjustFontScale(Configuration(newConfig))
    }

    private fun adjustFontScale(configuration: Configuration?) {
        if (configuration != null && configuration.fontScale.toDouble() != 1.0) {
            configuration.fontScale = 1.0.toFloat()
            val metrics = resources.displayMetrics
            val wm = activity!!.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            wm.defaultDisplay.getMetrics(metrics)
            metrics.scaledDensity = configuration.fontScale * metrics.density
            resources.updateConfiguration(configuration, metrics)
        }
    }
}
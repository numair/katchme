package app.grm.katchme.utils

import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.location.LocationManager
import android.net.ConnectivityManager
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.TextUtils
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.core.content.ContextCompat
import app.grm.katchme.R
import com.google.gson.Gson
import com.google.gson.stream.JsonReader
import okhttp3.Response
import java.io.StringReader
import java.lang.reflect.Type
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class Utils {
    fun getDayName(attendance_date: String?): String {
        val inFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        var date: Date? = null
        return try {
            date = inFormat.parse(attendance_date)
            Log.e("date", date.toString())
            val calendar = Calendar.getInstance()
            calendar.time = date
            val days = arrayOf("", "SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY")
            days[calendar[Calendar.DAY_OF_WEEK]]
            //            SimpleDateFormat outFormat = new SimpleDateFormat("EEEE", Locale.getDefault());
//            return outFormat.format(date);
        } catch (e: ParseException) {
            Log.e("parseException", e.localizedMessage)
            "null"
        }
    }

    fun getDayNameShort(attendance_date: String?): String {
        val inFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        var date: Date? = null
        return try {
            date = inFormat.parse(attendance_date)
            Log.e("date", date.toString())
            val calendar = Calendar.getInstance()
            calendar.time = date
            val days = arrayOf("", "SUN", "MON", "TUE", "WED", "THUR", "FRI", "SAT")
            days[calendar[Calendar.DAY_OF_WEEK]]
            //            SimpleDateFormat outFormat = new SimpleDateFormat("EEEE", Locale.getDefault());
//            return outFormat.format(date);
        } catch (e: ParseException) {
            Log.e("parseException", e.localizedMessage)
            "null"
        }
    }

    fun <T> getInternalParser(response: String?, type: Type?): T {
        val gson = Gson()
        val reader = JsonReader(StringReader(response))
        reader.isLenient = true
        return gson.fromJson(reader, type)
    }

    fun getAlertDialog(context: Context?, message: String?, title: String?): AlertDialog.Builder {
        val builder = AlertDialog.Builder(context)
        builder.setTitle(title)
        builder.setMessage(message)
        return builder
    }

    fun isNetworkAvailable(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    //    public static void actions(String text, Activity activity) {
    //        TextView textView = activity.findViewById(R.id.toolbar_tv);
    //        textView.setText(text);
    //        ImageView back = activity.findViewById(R.id.back);
    //        back.setOnTouchListener((view, motionEvent) -> {
    //            activity.finish();
    //            return false;
    //        });
    //
    //    }
    fun hideKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun showKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
    }

    fun anyFieldEmpty(strings: Array<String?>): Boolean {
        var isEmpty = false
        for (i in strings.indices) {
            if (TextUtils.isEmpty(strings[i])) {
                isEmpty = true
                break
            }
        }
        return isEmpty
    }

    fun getTimeFromDate(created_at: String?): String {
        var date: Date? = null
        try {
            date = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(created_at)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return SimpleDateFormat("hh:mm", Locale.getDefault()).format(date)
    }

    fun getDateTimeFromDate(created_at: String?): String {
        var date: Date? = null
        try {
            date = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(created_at)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault()).format(date)
    }

    fun getDateFromDate(created_at: String?): String {
        var date: Date? = null
        try {
            date = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(created_at)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(date)
    }

    fun getDayFromDate(created_at: String?): String {
        var date: Date? = null
        try {
            date = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(created_at)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return SimpleDateFormat("dd", Locale.getDefault()).format(date)
    }

    fun getMonthFromDate(created_at: String?): String {
        var date: Date? = null
        try {
            date = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(created_at)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return SimpleDateFormat("MMM", Locale.getDefault()).format(date)
    }

    companion object {
        @JvmStatic
        var instance = Utils()

        var dialog: ProgressDialog? = null
        fun showLoader(context: Context?, message: String?) {
            dialog = ProgressDialog(context)
            dialog!!.setMessage(message)
            dialog!!.show()
        }

        @JvmStatic
        fun getDateFromString(year: Int, month: Int, dayOfMonth: Int): Date? {
            try {
                return SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(year.toString() + "-" + (month + 1) + "-" + +dayOfMonth)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            return null
        }

        fun logResponse(key: String, response: Response?) {
            Log.e("response", "response of -> " + key + " is : " + Gson().toJson(response))
        }

        fun isGPSEnabled(context: Context): Boolean {
            val manager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
            return manager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        }

        //
        //    public void permission(Activity context, String permission, DexterPermissionListener dexterPermissionListener) {
        //        Dexter.withActivity(context)
        //                .withPermission(permission)
        //                .withListener(new PermissionListener() {
        //                    @Override
        //                    public void onPermissionGranted(PermissionGrantedResponse response) {
        //                        dexterPermissionListener.onGranted();
        //                    }
        //
        //                    @Override
        //                    public void onPermissionDenied(PermissionDeniedResponse response) {
        //                        dexterPermissionListener.onDenied();
        //                    }
        //
        //                    @Override
        //                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
        //                        token.continuePermissionRequest();
        //                    }
        //                }).onSameThread().check();
        //
        //    }
        //
        //
        //    public FusedLocationProviderClient getFusedClient(Context context) {
        //        FusedLocationProviderClient client = LocationServices.getFusedLocationProviderClient(context);
        //        return client;
        //    }
        fun dismissLoader() {
            if (dialog != null) dialog!!.dismiss()
        }

        fun policyTextView(ctx: Activity, view: TextView) {
            val spanTxt = SpannableStringBuilder("Ich habe die ")

            spanTxt.append("Nutzungsbedingungen ")
            spanTxt.setSpan(object : ClickableSpan() {
                override fun onClick(widget: View) {
                    //policyDialog(ctx, "Nutzungsbedingungen", "https://swapist.com/kunye/")
                    AppToast.show(ctx, "Nutzungsbedingungen")
                }
            }, spanTxt.length - "Nutzungsbedingungen".length - 1, spanTxt.length, 0)

            spanTxt.append("unddie ")
            spanTxt.append("Datenschutzerklärung ")
            spanTxt.setSpan(object : ClickableSpan() {
                override fun onClick(widget: View) {
//                    policyDialog(ctx, "Kullanım Sözleşmesi", "https://swapist.com/sartlar-ve-kosullar/")
                    AppToast.show(ctx, "Datenschutzerklärung")
                }
            }, spanTxt.length - "Datenschutzerklärung".length - 1, spanTxt.length, 0)

            spanTxt.append(" von KatchME gelesen und akzeptiere sie.")

            view.movementMethod = LinkMovementMethod.getInstance()
            view.setText(spanTxt, TextView.BufferType.SPANNABLE)
        }
    }
}
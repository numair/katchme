package app.grm.katchme.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import app.grm.katchme.view.BarActivity;
import app.grm.katchme.R;
import app.grm.katchme.model.Bar;
import xyz.klinker.android.drag_dismiss.DragDismissIntentBuilder;

public class SearchBarAdapter extends RecyclerView.Adapter<SearchBarAdapter.MYVH> {

    List<Bar> list = new ArrayList<>();
    Context context;

    public SearchBarAdapter(List<Bar> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public MYVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MYVH(LayoutInflater.from(parent.getContext()).inflate(R.layout.search_bar_view, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MYVH holder, int position) {
        holder.title.setText(list.get(holder.getAdapterPosition()).getTitle());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MYVH extends RecyclerView.ViewHolder {
        TextView title, spec, rating;
        ImageView imageView;

        public MYVH(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);

            itemView.setOnClickListener(view -> {
                // context.startActivity(new Intent(context, BarActivity.class));
                popView();
            });
        }
    }

    private void popView() {
        Intent dragDismissActivity = new Intent(context, BarActivity.class);
//
        new DragDismissIntentBuilder(context)
                .setTheme(DragDismissIntentBuilder.Theme.LIGHT)    // LIGHT (default), DARK, BLACK, DAY_NIGHT, SYSTEM_DEFAULT
                // .setPrimaryColorResource(R.color.colorPrimary)	// defaults to a semi-transparent black
                // defaults to null
                .setPrimaryColorResource(R.color.colorPrimary)    // defaults to a semi-transparent black
                .setToolbarTitle("Normal Activity Sample")
                .setShowToolbar(false)                // defaults to true
                .setShouldScrollToolbar(false)       // defaults to true
                .setFullscreenOnTablets(false)      // defaults to false, tablets will have padding on each side
                .setDragElasticity(DragDismissIntentBuilder.DragElasticity.NORMAL)  // Larger elasticities will make it easier to dismiss.
                .setDrawUnderStatusBar(false)       // defaults to false. Change to true if you don't want me to handle the content margin for the Activity. Does not apply to the RecyclerView Activities
                .build(dragDismissActivity);

// do anything else that you want to set up the Intent
// dragDismissActivity.putBoolean("test_bool", true);

        context.startActivity(dragDismissActivity);
    }
}


package app.grm.katchme.view.auth;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.afollestad.materialdialogs.MaterialDialog;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import app.grm.katchme.view.BaseActivity;
import app.grm.katchme.view.Dashboard;
import app.grm.katchme.R;
import app.grm.katchme.utils.MyClickableSpan;
import app.grm.katchme.utils.Utils;

public class RegisterActivity extends BaseActivity {

    private TextView checkBoxTv;
    private EditText dob, gender, userName, fullName, email;
    private String genders[] = new String[]{"Männlich", "Weiblich", "Divers"};
    private FrameLayout registerBtn;
    private CheckBox checkBox;
    private Boolean agreementByPass = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        this.initViews();
        this.dobListener();
        this.genderListener();
        this.validateListener();
        this.isAgreementLayerByPassed();


//        SpannableStringBuilder filteredText = this.getSpannableTextForCheckBox();
//        checkBoxTv.setMovementMethod(LinkMovementMethod.getInstance());   // make our spans selectable
//        checkBoxTv.setText(filteredText);
        //findViewById(R.id.checkbox_tv).
        Utils.Companion.policyTextView(this, findViewById(R.id.checkbox_tv));
    }

    private void isAgreementLayerByPassed() {
        checkBox.setOnCheckedChangeListener((compoundButton, b) -> {
            agreementByPass = b;
        });
    }

    private void validateListener() {

        registerBtn.setOnClickListener(view -> {
            if (!Utils.Companion.getInstance().anyFieldEmpty(new String[]{dob.getText().toString(), gender.getText().toString(), email.getText().toString(), userName.getText().toString(), fullName.getText().toString()})) {
                if (!agreementByPass) {
                    Toast.makeText(this, "Accept our terms and condition to proceed!", Toast.LENGTH_SHORT).show();
                    return;
                }

                Intent intent = new Intent(this, Dashboard.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);


            } else Toast.makeText(this, R.string.empty_fields, Toast.LENGTH_SHORT).show();
        });
    }

    private void genderListener() {

        gender.setOnClickListener(view -> {
            new MaterialDialog.Builder(this)
                    .title("Select Gender")
                    .items(genders)
                    .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                        @Override
                        public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                            /**
                             * If you use alwaysCallSingleChoiceCallback(), which is discussed below,
                             * returning false here won't allow the newly selected radio button to actually be selected.
                             **/
                            gender.setText(genders[which]);
                            return true;
                        }
                    })
                    .positiveText("Select")
                    .show();
        });
    }

    private void dobListener() {
        dob.setOnClickListener(v -> {

            Calendar calendar = Calendar.getInstance();

            new DatePickerDialog(this, (view, year, month, dayOfMonth) -> {
                Date date = Utils.getDateFromString(year, month, dayOfMonth);
                dob.setText(new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(date));
            }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
        });
    }

    private void initViews() {
        checkBoxTv = findViewById(R.id.checkbox_tv);
        dob = findViewById(R.id.dob);
        gender = findViewById(R.id.gender);
        userName = findViewById(R.id.user_name);
        registerBtn = findViewById(R.id.register);
        fullName = findViewById(R.id.full_name);
        email = findViewById(R.id.email);
        checkBox = findViewById(R.id.checkbox);
    }

    private void addClickableText(SpannableStringBuilder ssb, int startPos, String clickableText, String toastText) {
        ssb.append(clickableText);
        ssb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.controllerColorBlue)), startPos, ssb.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        ssb.setSpan(new MyClickableSpan(toastText), startPos, ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
    }

}
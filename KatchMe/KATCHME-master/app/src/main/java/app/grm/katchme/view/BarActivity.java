package app.grm.katchme.view;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import app.grm.katchme.R;
import app.grm.katchme.fragments.ReviewFragment;
import app.grm.katchme.fragments.StoriesFragment;
import app.grm.katchme.fragments.TagStoriesFragment;
import xyz.klinker.android.drag_dismiss.activity.DragDismissActivity;

public class BarActivity extends DragDismissActivity {
    TextView storiesTv, reviewTv, tagTv, logoutTv;


    @Override
    public View onCreateContent(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.activity_bar, parent, false);

        tagTv = view.findViewById(R.id.tagStoriesTv);
        storiesTv = view.findViewById(R.id.storiesTv);
        reviewTv = view.findViewById(R.id.reviewTv);

        switchTvAndFragment("review");
        tagTv.setOnClickListener(view1 -> {
            switchTvAndFragment("tag");
        });
        storiesTv.setOnClickListener(view12 -> {
            switchTvAndFragment("stories");
        });
        reviewTv.setOnClickListener(view13 -> {
            switchTvAndFragment("review");
        });
        return view;
    }

    private void switchTvAndFragment(String type) {

        if (type.equals("review")) {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new ReviewFragment())
                    .commit();
            reviewTv.setTextColor(Color.parseColor("#000000"));
            reviewTv.setTextSize(25);
            reviewTv.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/din_bold.ttf"));

            storiesTv.setTextColor(Color.parseColor("#E0E0E0"));
            storiesTv.setTextSize(17);
            storiesTv.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/dinregular.ttf"));

            // storiesTv.setTypeface(ResourcesCompat.getFont(context, R.font.dinregular));

            tagTv.setTextColor(Color.parseColor("#E0E0E0"));
            tagTv.setTextSize(17);
            tagTv.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/dinregular.ttf"));

            // tagTv.setTypeface(ResourcesCompat.getFont(context, R.font.dinregular));


        } else if (type.equals("stories")) {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new StoriesFragment())
                    .commit();
            storiesTv.setTextColor(Color.parseColor("#000000"));
            storiesTv.setTextSize(25);
            storiesTv.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/din_bold.ttf"));

//            storiesTv.setTypeface(ResourcesCompat.getFont(context, R.font.din_bold));

            reviewTv.setTextColor(Color.parseColor("#E0E0E0"));
            reviewTv.setTextSize(17);
            reviewTv.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/dinregular.ttf"));

            // reviewTv.setTypeface(ResourcesCompat.getFont(context, R.font.dinregular));


            tagTv.setTextColor(Color.parseColor("#E0E0E0"));
            tagTv.setTextSize(17);
            tagTv.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/dinregular.ttf"));

            // tagTv.setTypeface(ResourcesCompat.getFont(context, R.font.dinregular));

        } else {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new TagStoriesFragment())
                    .commit();
            tagTv.setTextColor(Color.parseColor("#000000"));
            tagTv.setTextSize(25);
            tagTv.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/din_bold.ttf"));

            // tagTv.setTypeface(ResourcesCompat.getFont(context, R.font.din_bold));
            reviewTv.setTextColor(Color.parseColor("#E0E0E0"));
            reviewTv.setTextSize(17);
            reviewTv.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/dinregular.ttf"));

            // reviewTv.setTypeface(ResourcesCompat.getFont(context, R.font.dinregular));
            storiesTv.setTextColor(Color.parseColor("#E0E0E0"));
            storiesTv.setTextSize(17);
            storiesTv.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/dinregular.ttf"));

            //storiesTv.setTypeface(ResourcesCompat.getFont(context, R.font.dinregular));

        }
    }

}
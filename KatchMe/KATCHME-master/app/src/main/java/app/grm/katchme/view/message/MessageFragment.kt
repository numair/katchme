package app.grm.katchme.view.message

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.grm.katchme.BaseFragment
import app.grm.katchme.R
import app.grm.katchme.adapters.ChatAdapter

/**
 * Created by Numair Qadir on 18/10/2020.
 */
class MessageFragment : BaseFragment() {

    lateinit var recyclerView: RecyclerView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.activity_chat, container, false)
        recyclerView = view!!.findViewById(R.id.chat_list)
        // Inflate the layout for this fragment
        initRec()

        return view
    }

    private fun initRec() {
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = ChatAdapter(context)
    }
}
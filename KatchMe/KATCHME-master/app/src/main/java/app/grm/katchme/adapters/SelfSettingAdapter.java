package app.grm.katchme.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import app.grm.katchme.R;
import app.grm.katchme.model.SelfSetting;

public class SelfSettingAdapter extends RecyclerView.Adapter<SelfSettingAdapter.MYVH> {

    Context context;
    List<SelfSetting> settings = new ArrayList<>();

    public SelfSettingAdapter(Context context, List<SelfSetting> settings) {
        this.context = context;
        this.settings = settings;
    }

    @NonNull
    @Override
    public MYVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MYVH(LayoutInflater.from(parent.getContext()).inflate(R.layout.self_setting_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MYVH holder, int position) {
        holder.tv.setText(settings.get(holder.getAdapterPosition()).getText());
    }

    @Override
    public int getItemCount() {
        return settings.size();
    }

    public class MYVH extends RecyclerView.ViewHolder {
        TextView tv;

        public MYVH(@NonNull View itemView) {
            super(itemView);
            tv = itemView.findViewById(R.id.tv);
        }
    }
}

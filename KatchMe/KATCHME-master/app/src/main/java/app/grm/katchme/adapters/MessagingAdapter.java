package app.grm.katchme.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import app.grm.katchme.R;

public class MessagingAdapter extends RecyclerView.Adapter<MessagingAdapter.MYVH> {
    @NonNull
    @Override
    public MYVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        MYVH MYVH = null;
        if (viewType == 0) {
            MYVH = new MYVH(LayoutInflater.from(parent.getContext()).inflate(R.layout.outgoing_chat_view, parent, false));
            return MYVH;
        } else {
            MYVH = new MYVH(LayoutInflater.from(parent.getContext()).inflate(R.layout.incoming_chat_view, parent, false));
            return MYVH;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull MYVH holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 20;
    }

    @Override
    public int getItemViewType(int position) {
        return position % 2 == 0 ? 0 : 1;
    }

    public class MYVH extends RecyclerView.ViewHolder {
        public MYVH(@NonNull View itemView) {
            super(itemView);
        }
    }
}

package app.grm.katchme.view.camera

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.grm.katchme.BaseFragment
import app.grm.katchme.R
import com.otaliastudios.cameraview.CameraView

//https://natario1.github.io/CameraView/about/getting-started
class CameraFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_camera, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val camera: CameraView = view.findViewById(R.id.camera)
        camera.setLifecycleOwner(viewLifecycleOwner)
    }
}
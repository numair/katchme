package app.grm.katchme.view.message;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import app.grm.katchme.R;
import app.grm.katchme.adapters.MessagingAdapter;
import app.grm.katchme.view.BaseActivity;

public class MessagingActivity extends BaseActivity {

    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messaging);
        recyclerView = findViewById(R.id.messaging_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new MessagingAdapter());

        findViewById(R.id.back)
                .setOnClickListener(view -> {
                    finish();
                });
    }

}
package app.grm.katchme.view.message;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import app.grm.katchme.R;
import app.grm.katchme.adapters.ChatAdapter;
import xyz.klinker.android.drag_dismiss.activity.DragDismissActivity;

public class ChatActivity extends DragDismissActivity {

    RecyclerView recyclerView;

    @Override
    public View onCreateContent(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
      final View  view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_chat, parent, false);
        recyclerView = view.findViewById(R.id.chat_list);
        initRec();
        return view;
    }

    private void initRec() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new ChatAdapter(this));
    }
}
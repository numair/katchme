package app.grm.katchme.view.auth

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import app.grm.katchme.R
import app.grm.katchme.utils.AppLog
import app.grm.katchme.view.BaseActivity
import app.grm.katchme.view.Dashboard
import com.facebook.AccessToken
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.facebook.login.widget.LoginButton
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.TwitterAuthProvider
import com.twitter.sdk.android.core.*
import com.twitter.sdk.android.core.identity.TwitterLoginButton
import com.twitter.sdk.android.core.models.User

class LoginActivity : BaseActivity() {

    // Firebase
    var mAuth: FirebaseAuth? = null

    // Twitter Object
    lateinit var twitterAuthConfig: TwitterAuthConfig
    lateinit var twitterConfig: TwitterConfig

    // Facebook Call Manager
    private var loginManager: LoginManager? = null
    private var mCallbackManager: CallbackManager? = null
    lateinit var iv_login_fb: LoginButton
    lateinit var imgFacebook: ImageView

    // UI
    lateinit var iv_login_twitter: TwitterLoginButton
    lateinit var imgTwitter: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        twitterAuthConfig = TwitterAuthConfig(getString(R.string.TWITTER_KEY), getString(R.string.TWITTER_SECRET))
        twitterConfig = TwitterConfig.Builder(this)
                .logger(DefaultLogger(Log.DEBUG))
                .twitterAuthConfig(twitterAuthConfig)
                .debug(true)
                .build()
        Twitter.initialize(twitterConfig)

        setContentView(R.layout.activity_login)

        //Init
        mAuth = FirebaseAuth.getInstance()
        imgTwitter = findViewById(R.id.imgTwitter)
        iv_login_twitter = findViewById(R.id.iv_login_twitter)
        imgFacebook = findViewById(R.id.imgFacebook)
        iv_login_fb = findViewById(R.id.iv_login_fb)

        findViewById<View>(R.id.register_tv)
                .setOnClickListener { view: View? -> startActivity(Intent(this, RegisterActivity::class.java)) }

        findViewById<View>(R.id.login)
                .setOnClickListener { view: View? ->
                    goToHome()
                }

        findViewById<View>(R.id.imgFacebook)
                .setOnClickListener { view: View? ->
                    iv_login_fb.performClick()
                }
        findViewById<View>(R.id.iv_login_fb)
                .setOnClickListener { view: View? ->
                    facebookLogin()
                }

        loginManager = LoginManager.getInstance()
        mCallbackManager = CallbackManager.Factory.create()

        twitterSetup()
    }

    private fun goToHome() {
        val intent = Intent(this, Dashboard::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Pass the activity result back to the Facebook SDK
        mCallbackManager?.onActivityResult(requestCode, resultCode, data)
    }

    /************************* Facebook - Start ************************/
    private fun facebookLogin() {
        // Callback registration
        iv_login_fb.registerCallback(mCallbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                // App code
                handleFacebookAccessToken(loginResult.accessToken)
            }

            override fun onCancel() {
                // App code
                AppLog.error("cancelled")
            }

            override fun onError(exception: FacebookException) {
                // App code
                AppLog.error("exception >>> $exception")
            }
        })
    }

    private fun handleFacebookAccessToken(token: AccessToken) {
        AppLog.error("handleFacebookAccessToken:$token")

        val credential = FacebookAuthProvider.getCredential(token.token)
        mAuth?.signInWithCredential(credential)
                ?.addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        AppLog.error("signInWithCredential:success")
                        val user = mAuth?.currentUser
                        AppLog.error(user.toString())
                        // TODO:
                        //openHomeActivity()

                        goToHome()
                        //updateUserInfoToServer(user, Common.FACEBOOK)
                        //                          updateUI(user);
                    } else {
                        // If sign in fails, display a message to the user.
                        AppLog.error("signInWithCredential:failure: " + task.exception)
                        //                          updateUI(null);
                    }
                }
    }

    /************************* Facebook - End *************************/

    /************************* Twitter - Start *************************/

    private fun twitterSetup() {
        AppLog.error("twitterLogin tapped")
        imgTwitter.setOnClickListener {
//            if (NetworkManager.isConnectedToNetwork(this)) {
            iv_login_twitter.performClick()
//            } else {
//                AppToast.show(this, getString(com.twitter.sdk.android.core.R.string.error_network))
//            }
        }

        iv_login_twitter.callback = object : Callback<TwitterSession>() {
            override fun success(result: Result<TwitterSession>) {
                AppLog.error("twitterLogin:success$result")
                //handleTwitterSession(result.data)
                getTwitterUserProfileWthTwitterCoreApi(this@LoginActivity, result.data)
            }

            override fun failure(exception: TwitterException) {
                AppLog.error("twitterLogin:failure: $exception")
            }
        }
    }

    private fun handleTwitterSession(session: TwitterSession) {
        AppLog.error("handleTwitterSession:$session")
        val credential = TwitterAuthProvider.getCredential(
                session.authToken.token,
                session.authToken.secret)
        mAuth!!.signInWithCredential(credential)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        AppLog.error("signInWithCredential:success")
                        val user = mAuth!!.currentUser
                        AppLog.error("" + user.toString())
                        goToHome()

                        //updateUserInfoToServer(user, Common.TWITTER)
                    } else {
                        // If sign in fails, display a message to the user.
                        AppLog.error("signInWithCredential:failure> " + task.exception)
                        Toast.makeText(this@LoginActivity, "Authentication failed.",
                                Toast.LENGTH_SHORT).show()
                    }
                }
    }

    fun getTwitterUserProfileWthTwitterCoreApi(
            context: Context, session: TwitterSession) {

        TwitterCore.getInstance().getApiClient(session).accountService
                .verifyCredentials(true, true, false)
                .enqueue(object : Callback<User>() {
                    override fun success(result: Result<User>) {
                        val name = result.data.name
                        val userName = result.data.screenName
                        val profileImageUrl = result.data.profileImageUrl.replace("_normal", "")
//                        val user = UserModel(name, userName, profileImageUrl, SocialNetwork.Twitter)
//                        startShareActivity(context, user)
                        AppLog.error("$name | $userName | $profileImageUrl")

                        handleTwitterSession(session)
                    }

                    override fun failure(exception: TwitterException) {
                        Toast.makeText(context, exception.localizedMessage, Toast.LENGTH_SHORT).show()
                    }
                })
    }

    /************************* Twitter - End *************************/
}
package app.grm.katchme.model;

public class Bar {
    String title,spec,rating,image;

    public Bar(String title, String spec, String rating, String image) {
        this.title = title;
        this.spec = spec;
        this.rating = rating;
        this.image = image;
    }

    public Bar() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}

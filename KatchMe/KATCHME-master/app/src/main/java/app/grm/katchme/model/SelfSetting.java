package app.grm.katchme.model;

public class SelfSetting {
    String text;
    int id;

    public SelfSetting(String text, int id) {
        this.text = text;
        this.id = id;
    }

    public SelfSetting() {
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

//package app.grm.katchme.view;
//
//import androidx.recyclerview.widget.LinearLayoutManager;
//import androidx.recyclerview.widget.RecyclerView;
//
//import android.os.Bundle;
//import android.os.Handler;
//import android.text.Editable;
//import android.text.TextWatcher;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.ProgressBar;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import app.grm.katchme.R;
//import app.grm.katchme.adapters.SearchBarAdapter;
//import app.grm.katchme.model.Bar;
//import xyz.klinker.android.drag_dismiss.activity.DragDismissActivity;
//
//public class SearchActivity extends DragDismissActivity {
//
//    ImageView search;
//    RecyclerView beforeSearchPopularBarList, beforeSearchHistoryList, afterSearchResultList;
//    ProgressBar progressBar;
//    EditText searchBar;
//    LinearLayout before, after;
//
//    List<Bar> popularList = new ArrayList<>();
//    List<Bar> historyList = new ArrayList<>();
//    List<Bar> searchList = new ArrayList<>();
//    //List<Bar> filterList = new ArrayList<>();
////
////    @Override
////    protected void onCreate(Bundle savedInstanceState) {
////        super.onCreate(savedInstanceState);
////        prepareMockData();
////        setContentView(R.layout.activity_search);
////
////
////        //if()
////
////
////    }
//
//
//    private void mockServerCall() {
//        setState("search");
//        progressBar.setVisibility(View.VISIBLE);
//        afterSearchResultList.setVisibility(View.GONE);
//        new Handler().postDelayed(() -> {
//            progressBar.setVisibility(View.GONE);
//            afterSearchResultList.setVisibility(View.VISIBLE);
//
//            searchList.add(new Bar("SEARCHED 1", "", "", ""));
//            searchList.add(new Bar("SEARCHED 2", "", "", ""));
//
//            afterSearchResultList.getAdapter().notifyDataSetChanged();
//        }, 1000);
//    }
//
//    private void setState(String state) {
//        if (state.equals("initial")) {
//            after.setVisibility(View.GONE);
//            before.setVisibility(View.VISIBLE);
//
//            beforeSearchPopularBarList.setVisibility(View.VISIBLE);
//            beforeSearchHistoryList.setVisibility(View.VISIBLE);
//
//        } else if (state.equals("all_hide")) {
//            Log.e("here", "here");
//            if (searchBar.getText().toString().length() <= 0) {
//                after.setVisibility(View.GONE);
//                before.setVisibility(View.GONE);
//
//            }
//        } else if (state.equals("search")) {
//            after.setVisibility(View.VISIBLE);
//            before.setVisibility(View.GONE);
//        }
//    }
//
//
//    private void prepareMockData() {
//        popularList.add(new Bar("Sun Bar", "Asia", "3", ""));
//        popularList.add(new Bar("TEST 1", "Asia", "3", ""));
//        popularList.add(new Bar("TEST 2", "Asia", "3", ""));
//        popularList.add(new Bar("TEST 3", "Asia", "3", ""));
//        popularList.add(new Bar("TEST 3", "Asia", "3", ""));
//        popularList.add(new Bar("TEST 3", "Asia", "3", ""));
//        popularList.add(new Bar("TEST 3", "Asia", "3", ""));
//
//        historyList.add(new Bar("TEST 4", "Asia", "3", ""));
//        historyList.add(new Bar("TEST 6", "Asia", "3", ""));
//        historyList.add(new Bar("TEST 5", "Asia", "3", ""));
//        historyList.add(new Bar("TEST 5", "Asia", "3", ""));
//        historyList.add(new Bar("TEST 5", "Asia", "3", ""));
//        historyList.add(new Bar("TEST 5", "Asia", "3", ""));
//        historyList.add(new Bar("TEST 5", "Asia", "3", ""));
//        historyList.add(new Bar("TEST 5", "Asia", "3", ""));
//        historyList.add(new Bar("TEST 5", "Asia", "3", ""));
//        historyList.add(new Bar("TEST 5", "Asia", "3", ""));
//    }
//
//    @Override
//    public View onCreateContent(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
//        View view1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_search, parent, false);
//        prepareMockData();
//        before = view1.findViewById(R.id.before_search_view);
//        after = view1.findViewById(R.id.after_search_view);
//        searchBar = view1.findViewById(R.id.search_bar);
//        search = view1.findViewById(R.id.search);
//        beforeSearchHistoryList = view1.findViewById(R.id.recent_before_search_rec);
//        beforeSearchPopularBarList = view1.findViewById(R.id.popular_before_search_rec);
//        progressBar = view1.findViewById(R.id.progressBar);
//        afterSearchResultList = view1.findViewById(R.id.after_search_rec);
//
//        beforeSearchHistoryList.setLayoutManager(new LinearLayoutManager(this));
//        beforeSearchPopularBarList.setLayoutManager(new LinearLayoutManager(this));
//        afterSearchResultList.setLayoutManager(new LinearLayoutManager(this));
//        beforeSearchHistoryList.setAdapter(new SearchBarAdapter(historyList, this));
//        beforeSearchPopularBarList.setAdapter(new SearchBarAdapter(popularList, this));
//        afterSearchResultList.setAdapter(new SearchBarAdapter(searchList, this));
//
//        setState("initial");
//
//        search.setOnClickListener(view -> {
//            if (searchBar.getText().toString().length() <= 0)
//                return;
//            mockServerCall();
//        });
////        searchBar.setOnClickListener(view -> {
////           // setState("all_hide");
////        });
//        searchBar.setOnFocusChangeListener((view, b) -> {
//            if (b) {
//
//                setState("all_hide");
//            }
//        });
//
//        searchBar.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                if (charSequence.length() == 0)
//                    setState("initial");
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//
//            }
//        });
//
//        return view1;
//    }
//}
package app.grm.katchme.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import app.grm.katchme.view.message.MessagingActivity;
import app.grm.katchme.R;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.MYVH> {
    Context context;

    public ChatAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public MYVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MYVH(LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_list_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MYVH holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class MYVH extends RecyclerView.ViewHolder {
        public MYVH(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(view -> {
                context.startActivity(new Intent(context, MessagingActivity.class));
            });
        }
    }
}

package app.grm.katchme.view;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;

import com.luseen.spacenavigation.SpaceItem;
import com.luseen.spacenavigation.SpaceNavigationView;
import com.luseen.spacenavigation.SpaceOnClickListener;

import app.grm.katchme.R;
import app.grm.katchme.utils.AppLog;
import app.grm.katchme.view.camera.CameraFragment;
import app.grm.katchme.view.home.HomeFragment;
import app.grm.katchme.view.message.ChatActivity;
import app.grm.katchme.view.message.MessageFragment;
import app.grm.katchme.view.profile.ProfileFragment;
import app.grm.katchme.view.search.SearchFragment;
import xyz.klinker.android.drag_dismiss.DragDismissIntentBuilder;

public class Dashboard extends BaseActivity {

    ImageView home, comment, camera, profile, search;
    private SpaceNavigationView spaceNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
//        this.bindBottomControllers();
//        this.bottomSheetState("home");
        //getSupportFragmentManager().beginTransaction().replace(R.id.container, new ProfileFragment()).commit();

        spaceNavigationView = findViewById(R.id.bottomBar);
        spaceNavigationView.initWithSaveInstanceState(savedInstanceState);
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_home));
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_loupe));
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_plaudern));
        spaceNavigationView.addSpaceItem(new SpaceItem("", R.drawable.ic_user));
        spaceNavigationView.shouldShowFullBadgeText(false);
        //spaceNavigationView.setCentreButtonIconColorFilterEnabled(false);
        spaceNavigationView.setCentreButtonIcon(R.drawable.ic_video_camera);
//        spaceNavigationView.setActiveCentreButtonBackgroundColor(getResources().getColor(android.R.color.transparent));
//        spaceNavigationView.setInActiveCentreButtonIconColor(getResources().getColor(android.R.color.transparent));

        spaceNavigationView.setSpaceOnClickListener(new SpaceOnClickListener() {
            @Override
            public void onCentreButtonClick() {
                AppLog.INSTANCE.error("onCentreButtonClick");
                spaceNavigationView.shouldShowFullBadgeText(true);
                switchFragment(new CameraFragment());
            }

            @Override
            public void onItemClick(int itemIndex, String itemName) {
                AppLog.INSTANCE.error("onItemClick: " + itemIndex + " " + itemName);
                switchViews(itemIndex);
            }

            @Override
            public void onItemReselected(int itemIndex, String itemName) {
                AppLog.INSTANCE.error("onItemReselected: " + itemIndex + " " + itemName);
                switchViews(itemIndex);
            }
        });

        AppLog.INSTANCE.error("dashvboard");

        switchViews(0);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        spaceNavigationView.onSaveInstanceState(outState);
    }

    public void switchFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.frame, fragment).commit();
    }

    public void switchViews(int position) {
        switch (position) {
            case 0:
                switchFragment(new HomeFragment());
                break;
            case 1:
                switchFragment(new SearchFragment());
                break;
            case 2:
                switchFragment(new MessageFragment());
                break;
            case 3:
                switchFragment(new ProfileFragment());
                break;
        }

    }

    public void showBottomView(){
        spaceNavigationView.setVisibility(View.VISIBLE);
    }

    public void hideBottomView(){
        spaceNavigationView.setVisibility(View.GONE);
    }
//    private void bottomSheetState(String state) {
//        if (state.equals("home")) {
//            home.setColorFilter(Color.parseColor("#E6611A"));
//            search.setColorFilter(Color.parseColor("#000000"));
//            comment.setColorFilter(Color.parseColor("#000000"));
//            camera.setColorFilter(Color.parseColor("#000000"));
//            profile.setColorFilter(Color.parseColor("#000000"));
//        } else if (state.equals("comment")) {
//            home.setColorFilter(Color.parseColor("#000000"));
//            search.setColorFilter(Color.parseColor("#000000"));
//            comment.setColorFilter(Color.parseColor("#E6611A"));
//            camera.setColorFilter(Color.parseColor("#000000"));
//            profile.setColorFilter(Color.parseColor("#000000"));
//
//            popView("comment");
//        } else if (state.equals("camera")) {
//            home.setColorFilter(Color.parseColor("#000000"));
//            search.setColorFilter(Color.parseColor("#000000"));
//            comment.setColorFilter(Color.parseColor("#000000"));
//            camera.setColorFilter(Color.parseColor("#E6611A"));
//            profile.setColorFilter(Color.parseColor("#000000"));
//
//            popView("camera");
//        } else if (state.equals("profile")) {
//            home.setColorFilter(Color.parseColor("#000000"));
//            search.setColorFilter(Color.parseColor("#000000"));
//            comment.setColorFilter(Color.parseColor("#000000"));
//            camera.setColorFilter(Color.parseColor("#000000"));
//            profile.setColorFilter(Color.parseColor("#E6611A"));
//
//
//            popView("profile");
//        } else {
//            home.setColorFilter(Color.parseColor("#000000"));
//            search.setColorFilter(Color.parseColor("#E6611A"));
//            comment.setColorFilter(Color.parseColor("#000000"));
//            camera.setColorFilter(Color.parseColor("#000000"));
//            profile.setColorFilter(Color.parseColor("#000000"));
//
//            popView("search");
//        }
//    }
//
//    private void popView(String viewType) {
//        if (viewType.equals("profile")) {
//            Intent dragDismissActivity = new Intent(this, UserProfileActivity.class);
////
//            new DragDismissIntentBuilder(this)
//                    .setTheme(DragDismissIntentBuilder.Theme.LIGHT)    // LIGHT (default), DARK, BLACK, DAY_NIGHT, SYSTEM_DEFAULT
//                    .setPrimaryColorResource(R.color.colorPrimary)    // defaults to a semi-transparent black
//                    .setToolbarTitle("Normal Activity Sample")        // defaults to null
//                    .setShowToolbar(false)                // defaults to true
//                    .setShouldScrollToolbar(false)       // defaults to true
//                    .setFullscreenOnTablets(false)      // defaults to false, tablets will have padding on each side
//                    .setDragElasticity(DragDismissIntentBuilder.DragElasticity.NORMAL)  // Larger elasticities will make it easier to dismiss.
//                    .setDrawUnderStatusBar(false)       // defaults to false. Change to true if you don't want me to handle the content margin for the Activity. Does not apply to the RecyclerView Activities
//                    .build(dragDismissActivity);
//
//// do anything else that you want to set up the Intent
//// dragDismissActivity.putBoolean("test_bool", true);
//
//            startActivity(dragDismissActivity);
//        } else if (viewType.equals("comment")) {
//            Intent dragDismissActivity = new Intent(this, ChatActivity.class);
////
//            new DragDismissIntentBuilder(this)
//                    .setTheme(DragDismissIntentBuilder.Theme.LIGHT)    // LIGHT (default), DARK, BLACK, DAY_NIGHT, SYSTEM_DEFAULT
//                    // .setPrimaryColorResource(R.color.colorPrimary)	// defaults to a semi-transparent black
//                    // defaults to null
//                    .setPrimaryColorResource(R.color.colorPrimary)    // defaults to a semi-transparent black
//                    .setToolbarTitle("Normal Activity Sample")
//                    .setShowToolbar(false)                // defaults to true
//                    .setShouldScrollToolbar(false)       // defaults to true
//                    .setFullscreenOnTablets(false)      // defaults to false, tablets will have padding on each side
//                    .setDragElasticity(DragDismissIntentBuilder.DragElasticity.NORMAL)  // Larger elasticities will make it easier to dismiss.
//                    .setDrawUnderStatusBar(false)       // defaults to false. Change to true if you don't want me to handle the content margin for the Activity. Does not apply to the RecyclerView Activities
//                    .build(dragDismissActivity);
//
//// do anything else that you want to set up the Intent
//// dragDismissActivity.putBoolean("test_bool", true);
//
//            startActivity(dragDismissActivity);
//        } else if (viewType.equals("search")) {
//            Intent dragDismissActivity = new Intent(this, SearchActivity.class);
////
//            new DragDismissIntentBuilder(this)
//                    .setTheme(DragDismissIntentBuilder.Theme.LIGHT)    // LIGHT (default), DARK, BLACK, DAY_NIGHT, SYSTEM_DEFAULT
//                    // .setPrimaryColorResource(R.color.colorPrimary)	// defaults to a semi-transparent black
//                    // defaults to null
//                    .setPrimaryColorResource(R.color.colorPrimary)    // defaults to a semi-transparent black
//                    .setToolbarTitle("Normal Activity Sample")
//                    .setShowToolbar(false)                // defaults to true
//                    .setShouldScrollToolbar(false)       // defaults to true
//                    .setFullscreenOnTablets(false)      // defaults to false, tablets will have padding on each side
//                    .setDragElasticity(DragDismissIntentBuilder.DragElasticity.NORMAL)  // Larger elasticities will make it easier to dismiss.
//                    .setDrawUnderStatusBar(false)       // defaults to false. Change to true if you don't want me to handle the content margin for the Activity. Does not apply to the RecyclerView Activities
//                    .build(dragDismissActivity);
//
//// do anything else that you want to set up the Intent
//// dragDismissActivity.putBoolean("test_bool", true);
//
//            startActivity(dragDismissActivity);
//        } else if (viewType.equals("camera")) {
//            Intent dragDismissActivity = new Intent(this, RadarActivity.class);
//            startActivity(dragDismissActivity);
//        }
//    }
//
//    private void bindBottomControllers() {
//        home = findViewById(R.id.home);
//        comment = findViewById(R.id.comment);
//        camera = findViewById(R.id.camera);
//        profile = findViewById(R.id.profile);
//        search = findViewById(R.id.search);
//
//        home.setOnClickListener(view -> {
//            this.bottomSheetState("home");
//        });
//
//        search.setOnClickListener(view -> {
//            this.bottomSheetState("search");
//        });
//
//        comment.setOnClickListener(view -> {
//            this.bottomSheetState("comment");
//        });
//
//        profile.setOnClickListener(view -> {
//            this.bottomSheetState("profile");
//        });
//
//        camera.setOnClickListener(view -> {
//            this.bottomSheetState("camera");
//        });
//    }
}
package app.grm.katchme.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import app.grm.katchme.R;

public class StoriesAdapter extends RecyclerView.Adapter<StoriesAdapter.MYVH> {
    @NonNull
    @Override
    public MYVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MYVH(LayoutInflater.from(parent.getContext()).inflate(R.layout.simple_picture_layout, parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MYVH holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 21;
    }

    public class MYVH extends RecyclerView.ViewHolder {
        public MYVH(@NonNull View itemView) {
            super(itemView);
        }
    }
}

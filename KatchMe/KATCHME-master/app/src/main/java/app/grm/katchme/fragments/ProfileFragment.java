package app.grm.katchme.fragments;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import app.grm.katchme.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    TextView storiesTv, reviewTv, tagTv;
    private Context context;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public ProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfileFragment newInstance(String param1, String param2) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tagTv = view.findViewById(R.id.tagStoriesTv);
        storiesTv = view.findViewById(R.id.storiesTv);
        reviewTv = view.findViewById(R.id.reviewTv);

        switchTvAndFragment("review");
        tagTv.setOnClickListener(view1 -> {
            switchTvAndFragment("tag");
        });
        storiesTv.setOnClickListener(view12 -> {
            switchTvAndFragment("stories");
        });
        reviewTv.setOnClickListener(view13 -> {
            switchTvAndFragment("review");
        });
    }

    private void switchTvAndFragment(String type) {

        if (type.equals("review")) {
            getChildFragmentManager().beginTransaction().replace(R.id.container, new ReviewFragment())
                    .commit();
            reviewTv.setTextColor(Color.parseColor("#000000"));
            reviewTv.setTextSize(25);
            reviewTv.setTypeface(ResourcesCompat.getFont(context, R.font.din_bold));

            storiesTv.setTextColor(Color.parseColor("#E0E0E0"));
            storiesTv.setTextSize(17);
            storiesTv.setTypeface(ResourcesCompat.getFont(context, R.font.dinregular));

            tagTv.setTextColor(Color.parseColor("#E0E0E0"));
            tagTv.setTextSize(17);
            tagTv.setTypeface(ResourcesCompat.getFont(context, R.font.dinregular));


        } else if (type.equals("stories")) {
            getChildFragmentManager().beginTransaction().replace(R.id.container, new StoriesFragment())
                    .commit();
            storiesTv.setTextColor(Color.parseColor("#000000"));
            storiesTv.setTextSize(25);
            storiesTv.setTypeface(ResourcesCompat.getFont(context, R.font.din_bold));

            reviewTv.setTextColor(Color.parseColor("#E0E0E0"));
            reviewTv.setTextSize(17);
            reviewTv.setTypeface(ResourcesCompat.getFont(context, R.font.dinregular));


            tagTv.setTextColor(Color.parseColor("#E0E0E0"));
            tagTv.setTextSize(17);
            tagTv.setTypeface(ResourcesCompat.getFont(context, R.font.dinregular));

        } else {
            getChildFragmentManager().beginTransaction().replace(R.id.container, new TagStoriesFragment())
                    .commit();
            tagTv.setTextColor(Color.parseColor("#000000"));
            tagTv.setTextSize(25);
            tagTv.setTypeface(ResourcesCompat.getFont(context, R.font.din_bold));
            reviewTv.setTextColor(Color.parseColor("#E0E0E0"));
            reviewTv.setTextSize(17);
            reviewTv.setTypeface(ResourcesCompat.getFont(context, R.font.dinregular));
            storiesTv.setTextColor(Color.parseColor("#E0E0E0"));
            storiesTv.setTextSize(17);
            storiesTv.setTypeface(ResourcesCompat.getFont(context, R.font.dinregular));

        }
    }
}
package app.grm.katchme.view.search

import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.grm.katchme.BaseFragment
import app.grm.katchme.R
import app.grm.katchme.adapters.SearchBarAdapter
import app.grm.katchme.model.Bar

/**
 * Created by Numair Qadir on 18/10/2020.
 */
class SearchFragment : BaseFragment() {

    lateinit var search: ImageView
    lateinit var beforeSearchPopularBarList: RecyclerView
    lateinit var beforeSearchHistoryList:RecyclerView
    lateinit var afterSearchResultList:RecyclerView
    lateinit var progressBar: ProgressBar
    lateinit var searchBar: EditText
    lateinit var before: LinearLayout
    lateinit var after:android.widget.LinearLayout

    var popularList= arrayListOf<Bar>()
    var historyList= arrayListOf<Bar>()
    var searchList = arrayListOf<Bar>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_search, container, false)
        val view1 = inflater.inflate(R.layout.fragment_search, container, false)
        prepareMockData()

        before = view1.findViewById(R.id.before_search_view)
        after = view1.findViewById(R.id.after_search_view)
        searchBar = view1.findViewById(R.id.search_bar)
        search = view1.findViewById(R.id.search)
        beforeSearchHistoryList = view1.findViewById(R.id.recent_before_search_rec)
        beforeSearchPopularBarList = view1.findViewById(R.id.popular_before_search_rec)
        progressBar = view1.findViewById(R.id.progressBar)
        afterSearchResultList = view1.findViewById(R.id.after_search_rec)

        beforeSearchHistoryList.layoutManager = LinearLayoutManager(context)
        beforeSearchPopularBarList.layoutManager = LinearLayoutManager(context)
        afterSearchResultList.layoutManager = LinearLayoutManager(context)
        beforeSearchHistoryList.adapter = SearchBarAdapter(historyList, context)
        beforeSearchPopularBarList.adapter = SearchBarAdapter(popularList, context)
        afterSearchResultList.adapter = SearchBarAdapter(searchList, context)

        setState("initial")

        search.setOnClickListener { view: View? ->
            if (searchBar.text.toString().length <= 0) return@setOnClickListener
            mockServerCall()
        }
//        searchBar.setOnClickListener(view -> {
//           // setState("all_hide");
//        });
        //        searchBar.setOnClickListener(view -> {
//           // setState("all_hide");
//        });
        searchBar.onFocusChangeListener = View.OnFocusChangeListener { view: View?, b: Boolean ->
            if (b) {
                setState("all_hide")
            }
        }

        searchBar.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                if (charSequence.length == 0) setState("initial")
            }

            override fun afterTextChanged(editable: Editable) {}
        })

        return view1
    }


    //List<Bar> filterList = new ArrayList<>();
    //
    //    @Override
    //    protected void onCreate(Bundle savedInstanceState) {
    //        super.onCreate(savedInstanceState);
    //        prepareMockData();
    //        setContentView(R.layout.activity_search);
    //
    //
    //        //if()
    //
    //
    //    }
    private fun mockServerCall() {
        setState("search")
        progressBar.visibility = View.VISIBLE
        afterSearchResultList.visibility = View.GONE
        Handler().postDelayed({
            progressBar.visibility = View.GONE
            afterSearchResultList.visibility = View.VISIBLE
            searchList.add(Bar("SEARCHED 1", "", "", ""))
            searchList.add(Bar("SEARCHED 2", "", "", ""))
            afterSearchResultList.adapter!!.notifyDataSetChanged()
        }, 1000)
    }

    private fun setState(state: String) {
        if (state == "initial") {
            after.visibility = View.GONE
            before.visibility = View.VISIBLE
            beforeSearchPopularBarList.visibility = View.VISIBLE
            beforeSearchHistoryList.visibility = View.VISIBLE
        } else if (state == "all_hide") {
            Log.e("here", "here")
            if (searchBar.text.toString().length <= 0) {
                after.visibility = View.GONE
                before.visibility = View.GONE
            }
        } else if (state == "search") {
            after.visibility = View.VISIBLE
            before.visibility = View.GONE
        }
    }


    private fun prepareMockData() {
        popularList.add(Bar("Sun Bar", "Asia", "3", ""))
        popularList.add(Bar("TEST 1", "Asia", "3", ""))
        popularList.add(Bar("TEST 2", "Asia", "3", ""))
        popularList.add(Bar("TEST 3", "Asia", "3", ""))
        popularList.add(Bar("TEST 3", "Asia", "3", ""))
        popularList.add(Bar("TEST 3", "Asia", "3", ""))
        popularList.add(Bar("TEST 3", "Asia", "3", ""))
        historyList.add(Bar("TEST 4", "Asia", "3", ""))
        historyList.add(Bar("TEST 6", "Asia", "3", ""))
        historyList.add(Bar("TEST 5", "Asia", "3", ""))
        historyList.add(Bar("TEST 5", "Asia", "3", ""))
        historyList.add(Bar("TEST 5", "Asia", "3", ""))
        historyList.add(Bar("TEST 5", "Asia", "3", ""))
        historyList.add(Bar("TEST 5", "Asia", "3", ""))
        historyList.add(Bar("TEST 5", "Asia", "3", ""))
        historyList.add(Bar("TEST 5", "Asia", "3", ""))
        historyList.add(Bar("TEST 5", "Asia", "3", ""))
    }
}
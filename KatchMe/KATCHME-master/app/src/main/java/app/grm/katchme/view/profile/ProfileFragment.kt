package app.grm.katchme.view.profile

import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.grm.katchme.BaseFragment
import app.grm.katchme.R
import app.grm.katchme.adapters.SelfSettingAdapter
import app.grm.katchme.fragments.ReviewFragment
import app.grm.katchme.fragments.StoriesFragment
import app.grm.katchme.fragments.TagStoriesFragment
import app.grm.katchme.model.SelfSetting
import java.util.*

/**
 * Created by Numair Qadir on 18/10/2020.
 */
class ProfileFragment : BaseFragment() {

    lateinit var storiesTv: TextView
    lateinit var reviewTv: TextView
    lateinit var tagTv: TextView
    lateinit var logoutTv: TextView
    lateinit var popUpMenu: ImageView
    lateinit var settingsMenu: RelativeLayout
    lateinit var settingList: RecyclerView
    lateinit var back: ImageView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_profile, container, false)
        val view = inflater.inflate(R.layout.fragment_profile, container, false)
        tagTv = view.findViewById(R.id.tagStoriesTv)
        //this.context = parent.context
        storiesTv = view.findViewById(R.id.storiesTv)
        reviewTv = view.findViewById(R.id.reviewTv)
        popUpMenu = view.findViewById(R.id.popup_menu)
        settingsMenu = view.findViewById(R.id.settings_menu)
        logoutTv = view.findViewById(R.id.logout)
        back = view.findViewById(R.id.back)
        settingList = view.findViewById(R.id.settings)
        settingList.layoutManager = LinearLayoutManager(context)
        settingList.adapter = SelfSettingAdapter(context, getSettings())
        popUpMenu.setOnClickListener { view14: View? -> showActions() }
        back.setOnClickListener { view15: View? -> hideActions() }
        switchTvAndFragment("review")
        tagTv.setOnClickListener { view1: View? -> switchTvAndFragment("tag") }
        storiesTv.setOnClickListener { view12: View? -> switchTvAndFragment("stories") }
        reviewTv.setOnClickListener { view13: View? -> switchTvAndFragment("review") }

        return view
    }

    private fun showActions() {
        settingsMenu.visibility = View.VISIBLE
    }

    private fun hideActions() {
        settingsMenu.visibility = View.GONE
    }

    private fun getSettings(): List<SelfSetting>? {
        val selfSettings: MutableList<SelfSetting> = ArrayList()
        selfSettings.add(SelfSetting("PASSWORT", 1))
        selfSettings.add(SelfSetting("KONTO", 2))
        selfSettings.add(SelfSetting("BLOCKIERTE PERSONEN", 3))
        selfSettings.add(SelfSetting("SUCHVERLAUF LOSCHEN", 4))
        selfSettings.add(SelfSetting("HILFE / KONTAKT", 5))
        selfSettings.add(SelfSetting("DATENSCHUTD", 6))
        selfSettings.add(SelfSetting("IMPRESSUM", 7))
        return selfSettings
    }

    private fun switchTvAndFragment(type: String) {
        if (type == "review") {
            activity!!.supportFragmentManager.beginTransaction().replace(R.id.profileContainer, ReviewFragment())
                    .commit()
            reviewTv.setTextColor(Color.parseColor("#000000"))
            reviewTv.textSize = 25f
            reviewTv.typeface = Typeface.createFromAsset(context!!.assets, "fonts/din_bold.ttf")
            storiesTv.setTextColor(Color.parseColor("#E0E0E0"))
            storiesTv.textSize = 17f
            storiesTv.typeface = Typeface.createFromAsset(context!!.assets, "fonts/dinregular.ttf")

            // storiesTv.setTypeface(ResourcesCompat.getFont(context, R.font.dinregular));
            tagTv.setTextColor(Color.parseColor("#E0E0E0"))
            tagTv.textSize = 17f
            tagTv.typeface = Typeface.createFromAsset(context!!.assets, "fonts/dinregular.ttf")

            // tagTv.setTypeface(ResourcesCompat.getFont(context, R.font.dinregular));
        } else if (type == "stories") {
            activity!!.supportFragmentManager.beginTransaction().replace(R.id.profileContainer, StoriesFragment())
                    .commit()
            storiesTv.setTextColor(Color.parseColor("#000000"))
            storiesTv.textSize = 25f
            storiesTv.typeface = Typeface.createFromAsset(context!!.assets, "fonts/din_bold.ttf")

//            storiesTv.setTypeface(ResourcesCompat.getFont(context, R.font.din_bold));
            reviewTv.setTextColor(Color.parseColor("#E0E0E0"))
            reviewTv.textSize = 17f
            reviewTv.typeface = Typeface.createFromAsset(context!!.assets, "fonts/dinregular.ttf")

            // reviewTv.setTypeface(ResourcesCompat.getFont(context, R.font.dinregular));
            tagTv.setTextColor(Color.parseColor("#E0E0E0"))
            tagTv.textSize = 17f
            tagTv.typeface = Typeface.createFromAsset(context!!.assets, "fonts/dinregular.ttf")

            // tagTv.setTypeface(ResourcesCompat.getFont(context, R.font.dinregular));
        } else {
            activity!!.supportFragmentManager.beginTransaction().replace(R.id.profileContainer, TagStoriesFragment())
                    .commit()
            tagTv.setTextColor(Color.parseColor("#000000"))
            tagTv.textSize = 25f
            tagTv.typeface = Typeface.createFromAsset(context!!.assets, "fonts/din_bold.ttf")

            // tagTv.setTypeface(ResourcesCompat.getFont(context, R.font.din_bold));
            reviewTv.setTextColor(Color.parseColor("#E0E0E0"))
            reviewTv.textSize = 17f
            reviewTv.typeface = Typeface.createFromAsset(context!!.assets, "fonts/dinregular.ttf")

            // reviewTv.setTypeface(ResourcesCompat.getFont(context, R.font.dinregular));
            storiesTv.setTextColor(Color.parseColor("#E0E0E0"))
            storiesTv.textSize = 17f
            storiesTv.typeface = Typeface.createFromAsset(context!!.assets, "fonts/dinregular.ttf")

            //storiesTv.setTypeface(ResourcesCompat.getFont(context, R.font.dinregular));
        }
    }
}
package app.grm.katchme.view.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.grm.katchme.BaseFragment
import app.grm.katchme.R

/**
 * Created by Numair Qadir on 19/10/2020.
 */
class KatchFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_katch, container, false)
    }
}
package app.grm.katchme.utils

import android.content.Context
import android.widget.Toast

/**
 * Created by Numair Qadir on 03/12/2018.
 */
class AppToast {
    companion object {
        fun show(ctx: Context, message: String) {
            if (!message.isEmpty()) {
                Toast.makeText(ctx, message, Toast.LENGTH_SHORT).show()
            }
        }
    }
}
package app.grm.katchme.utils;

import android.graphics.Color;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.Toast;

public class MyClickableSpan extends ClickableSpan {
    String text;

   public MyClickableSpan(String text) {
        this.text = text;
    }

    @Override
    public void updateDrawState(TextPaint ds) {
        ds.setUnderlineText(false); // get rid of underlining
        ds.setColor(Color.parseColor("#E6611A"));     // make links red
    }

    @Override
    public void onClick(View view) {
        Toast.makeText(view.getContext(), text, Toast.LENGTH_SHORT).show();
    }
}